UOC - Graphic Programming - Aleix Cots Molina

PEC2: Pacman

*(source files are stored in "game/src/" folder, resources in "resources" folder)*

Link to video (in folder "Portfolio/Raylib(C++)/Pacman2D"): https://drive.google.com/drive/folders/1f19dK-otYpjN3iWk3WBaSh5HfGdSFySk?usp=sharing

===========================================================

The whole logic is split in two parts.

---------------------

On one hand, the program has a little "engine" implemented to do the heavy stuff that a game engine usually does, so the Pacman
game logic can be decoupled (mostly) from this. This little engine only implements the basic logic needed with the following classes:

	- Engine: Main static class. Allows to:
		- Create or destroy an instance of the given game.
		- Instantiate or destroy a new actor.
		- Update all the actors in the game, which includes: calling their start/update and draw methods so they can update themselves and implement
		  their own drawing; check all the collisions between actors and call their collision method so they can decide what 
		  to do (like Unity's OnCollisionStay/OnTriggerStay methods); and remove the actors that were destroyed (marked as inactive).
		- It also stores the actors in a list of lists, where each sublist contains all the actors with the same layer only, so there can be
		  a drawing order.
		  
	- Actor: Base abstract class that a class must implement if it is intended to be an actor of the game. It has a constructor that allows to specify
	  a layer (for drawing purposes) and five virtual methods that the derived class can implement if needed:
		- Start is called the first time that an actor is updated.
		- Update is called from the second time on, until it is destroyed.
		- Draw allows the actor decide what needs to be drawn in the screen.
		- GetBody allows the subclass to define a shape for the actor so Engine can check the collisions.
		- CollisionWith(Actor) notifies the actor about a collision with another actor.
		
	- ActorState: Helper class inside of Engine used as a wrapper for all the actors to provide further information about the actor only known
	  by the engine. In particular, it contains two Boolean:
		- isAlive indicates whether the actor can be definitely removed or not.
		- isStarted indicates if its Start method has been called.
		
---------------------

On the other hand, the logic of the Pacman game is divided as follows:

	- Pacman: Main class of the game. Works as a FSM of screens to switch between them.
	
	- Screens system:
		- ScreensManager: Interface that the manager of the screens (Paratrooper) must implement so they can ask to switch from one to another.
		- Screen: Base class that all screens must implement.
		- LogoScreen: Shows an image for a few seconds, then asks the manager to switch to TitleScreen.
		- TitleScreen: Acts as a main menu which allows the player to switch to OptionsScreen or start the game (GameplayScreen).
		- OptionsScreen: Shows a little information about the game and how to play. Switches to TitleScreen when the player hits "O".
		- GameplayScreen: Starts the game and updates the UI information. Eventually, when the game is over, will switch to EndingScreen.
		- EndingScreen: Last screen of the game, shows a win/lose message. Will allow the user to switch to TitleScreen or OptionsScreen.
		- ScreenData: Base class for any class intended to hold data for a screen.
		- EndingScreenData: Has a boolean that tells whether the player won or not, and is sent to the EndingScreen when it is created.
	
	- GameManager: Main class of the gameplay part. Instantiates the map, the player and the ghosts (only one in this Pacman), and keeps a track of them
	  so they can be reset after player's death and destroyed when the gameplay is over.
	  
	- Map: Holds all the data of the map (tiles, colliders, objects, etc.) and the methods to interact with this data.
	  
	- Player: Receives the input to move the Pacman all over the map.
	
	- Ghost: Base class used to represent a ghost and its general behaviour (common amongst all ghosts) and leave the especific behaviour of a ghost
	  to the derived class.
	
	- BlinkyGhost: Derived class from Ghost that implements the behaviour of the Blinky ghost (each ghost chases the player differently).
	
	- Tilemap: Base class used to to hold the data of a tile map and load its tiles given a specific format (tiles are numbers, including negatives,
	  anything else will be treated as a separator). It also forces the derived classes to calculate the rectangles of the sprites.
	
	- PacmanTilemap: Derived class from Tilemap that implements the load of the rectangles scorresponding to the sprites of the texture.
	
	- ResourcesManager: Static class that implements all the Load/Unload methods and all the external data for textures, audio and txt files:

---------------------
	
Raylib functions used:

	- Drawing:
		- Basic initializations/deactivations: InitWindow/CloseWindow, SetTargetFPS, LoadTexture/UnloadTexture and LoadFont/UnloadFont.
		- BeginDrawing and ClearBackground called before drawing the actors, and EndDrawing called right after.
		- DrawTextEx to draw a text with a certain font.
			- MeasureText to get the width of a text so it can be centered horizontally.
		
	- Sounds:
		- Basic initializations/deactivations: InitAudioDevice/CloseAudioDevice, LoadSound/UnloadSound and 
		  LoadMusicStream/UnloadMusicStream.
		- PlaySound/StopSound.
		- PlayMusicStream, SetMusicVolume, UpdateMusicStream and StopMusicStream to manage a music field.
		
	- Txt files:
	    - LoadTextFile to load the tile map data: tilemap, colliders and objects.
		
	- CheckCollisionRecs to check if two Rectangle collide.
	
---------------------

*Look over the classes and methods for further information about the implementation*

===========================================================