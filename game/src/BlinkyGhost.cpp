#include "BlinkyGhost.h"
#include "Map.h"
#include "ResourcesManager.h"
#include "Player.h"
#include <iostream>

Vector2 BlinkyGhost::GetStartPosition()
{
	return _map->TileToMapPosition(9, 13);
}
Vector2 BlinkyGhost::GetFrightenedPosition()
{
	// Up/right corner.
	return _map->TileToMapPosition(1, 25);
}
Vector2 BlinkyGhost::GetEatenPosition()
{
	// Center.
	return _map->TileToMapPosition(11, 13);
}

BlinkyGhost::BlinkyGhost(Map* map, Player* player) 
	: Ghost(ResourcesManager::EnemiesImage, map, player)
{
	_frightenedTargetPosition = GetFrightenedPosition();
	_eatenTargetPosition = GetEatenPosition();
}

void BlinkyGhost::InitChaseSprites(Texture2D source, map<Map::Direction, array<Rectangle, 2>>& chaseSprites)
{	
	int rows = 4;
	float cell = (int)(source.height / rows);

	chaseSprites[Map::Right] = {
		Rectangle { 0 * cell, 0 * cell, cell, cell },
		Rectangle { 1 * cell, 0 * cell, cell, cell }
	};
	chaseSprites[Map::Left] = {
		Rectangle { 2 * cell, 0 * cell, cell, cell },
		Rectangle { 3 * cell, 0 * cell, cell, cell }
	};
	chaseSprites[Map::Up] = {
		Rectangle { 4 * cell, 0 * cell, cell, cell },
		Rectangle { 5 * cell, 0 * cell, cell, cell }
	};
	chaseSprites[Map::Down] = {
		Rectangle { 6 * cell, 0 * cell, cell, cell },
		Rectangle { 7 * cell, 0 * cell, cell, cell }
	};
}

void BlinkyGhost::InitFrightenedSprites(Texture2D source, array<Rectangle, 4>& frightenedSprites)
{
	int rows = 4;
	float cell = (int)(source.height / rows);

	frightenedSprites = {
		Rectangle { 8  * cell, 0 * cell, cell, cell },
		Rectangle { 9  * cell, 0 * cell, cell, cell },
		Rectangle { 10 * cell, 0 * cell, cell, cell },
		Rectangle { 11 * cell, 0 * cell, cell, cell }
	};
}

void BlinkyGhost::InitEatenSprites(Texture2D source, map<Map::Direction, Rectangle>& eatenSprites)
{
	int rows = 4;
	float cell = (int)(source.height / rows);

	eatenSprites[Map::Right] = {
		Rectangle { 8 * cell, 1 * cell, cell, cell }
	};
	eatenSprites[Map::Left] = {
		Rectangle { 9 * cell, 1 * cell, cell, cell }
	};
	eatenSprites[Map::Up] = {
		Rectangle { 10 * cell, 1 * cell, cell, cell }
	};
	eatenSprites[Map::Down] = {
		Rectangle { 11 * cell, 1 * cell, cell, cell }
	};
}

using namespace std;

Map::Direction BlinkyGhost::ComputeNextDirectionChase(map<Map::Direction, Vector2> adjacents,
	Map::Direction previousDirection)
{
	// In Pacman, ghosts should never go back, so this option is removed
	// from the adjacent tiles.
	adjacents.erase(_map->GetOppositeDirection(previousDirection));

	// Eaten -> Chase case, when this list is empty because the ghost is 
	// in the center of the map and can only go Up.
	if (adjacents.empty())
	{
		return _map->GetOppositeDirection(previousDirection);
	}

	// Otherwise, find the best adjacent option given the target, which
	// is the Pacman's position.

	auto nextDir = adjacents.begin()->first;
	auto nextPos = adjacents.begin()->second;

	if (adjacents.size() > 1)
	{
		Vector2 targetPos = _player->GetPosition();
		float dist = Map::Distance(nextPos, targetPos);

		auto it = adjacents.begin();
		it++;
		for (it; it != adjacents.end(); it++)
		{
			float d = Map::Distance(it->second, targetPos);
			if (d < dist)
			{
				dist = d;
				nextDir = it->first;
				nextPos = it->second;
			}
		}
	}

	return nextDir;
}

Map::Direction BlinkyGhost::ComputeNextDirectionFrightened(map<Map::Direction, Vector2> adjacents,
	Map::Direction previousDirection)
{
	// Ghosts should never go back, so this option is removed from the
	// adjacent tiles.
	adjacents.erase(_map->GetOppositeDirection(previousDirection));

	// Find the best adjacent option given the target, which is the most
	// up/right corner of the map.

	auto nextDir = adjacents.begin()->first;
	auto nextPos = adjacents.begin()->second;

	if (adjacents.size() > 1)
	{
		Vector2 targetPos = _frightenedTargetPosition;
		float dist = Map::Distance(nextPos, targetPos);

		auto it = adjacents.begin();
		it++;
		for (it; it != adjacents.end(); it++)
		{
			float d = Map::Distance(it->second, targetPos);
			if (d < dist)
			{
				dist = d;
				nextDir = it->first;
				nextPos = it->second;
			}
		}
	}

	return nextDir;
}

Map::Direction BlinkyGhost::ComputeNextDirectionEaten(map<Map::Direction, Vector2> adjacents,
	Map::Direction previousDirection)
{
	// If there is only one option or none, it means that the ghost has
	// reached the center, so change to Chase state.
	if (adjacents.size() <= 1)
	{
		ChangeState(Ghost::Chase);
		return _map->GetOppositeDirection(previousDirection);
	}

	// Ghosts should never go back, so this option is removed from the
	// adjacent tiles.
	adjacents.erase(_map->GetOppositeDirection(previousDirection));

	// Find the best adjacent option given the target, which is the center.

	auto nextDir = adjacents.begin()->first;
	auto nextPos = adjacents.begin()->second;

	if (adjacents.size() > 1)
	{
		Vector2 targetPos = _eatenTargetPosition;
		float dist = Map::Distance(nextPos, targetPos);

		auto it = adjacents.begin();
		it++;
		for (it; it != adjacents.end(); it++)
		{
			float d = Map::Distance(it->second, targetPos);
			if (d < dist)
			{
				dist = d;
				nextDir = it->first;
				nextPos = it->second;
			}
		}
	}

	return nextDir;
}

void BlinkyGhost::CollisionWith(Actor* actor)
{
	
}

BlinkyGhost::~BlinkyGhost()
{
	cout << "GHOST DEAD\n";
}
