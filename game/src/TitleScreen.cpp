#include "TitleScreen.h"
#include "ResourcesManager.h"
#include "Pacman.h"

TitleScreen::TitleScreen(ScreensManager* manager, ScreenData* data) : Screen(manager)
{
	_text = "Author: Aleix Cots Molina\n"
		"Press 'Enter' to play\n"
		"Press 'O' to to open the Options menu\n";

	_image = ResourcesManager::TitleImage;
	_imageAspectRatio = (float)_image.height / (float)_image.width;
	_source = { 0, 0, (float)_image.width, (float)_image.height };
	_dest = { (float)(_width / 4), (float)(_height / 8), (float)(_width / 2), (float)(_width / 2) * _imageAspectRatio };

	PlaySound(ResourcesManager::IntroTheme);
}

void TitleScreen::Update()
{
	if (IsKeyReleased(KEY_ENTER))
	{
		_manager->SwitchTo(Pacman::Screens::GAMEPLAY);
	}
	else if (IsKeyReleased(KEY_O))
	{
		_manager->SwitchTo(Pacman::Screens::OPTIONS);
	}
}

void TitleScreen::Draw()
{
	ClearBackground(BLACK);

	DrawTexturePro(_image, _source, _dest, { 0, 0 }, 0, WHITE);
	DrawText(_text, _width / 3, _height / 2, _width / 45, WHITE);
}

TitleScreen::~TitleScreen()
{
	StopSound(ResourcesManager::IntroTheme);
}
