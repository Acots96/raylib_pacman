#pragma once
#include <raylib.h>
#include "ScreensManager.h"
#include <memory>
#include <map>
#include "Actor.h"

using namespace std;

// Main class of the game.
// Holds an instance of the active screen and allows
// to change to another screen.
class Pacman : public Actor, public ScreensManager
{
	public:
		enum Screens { 
			LOGO = 0,
			TITLE = 1,
			OPTIONS = 2,
			GAMEPLAY = 3,
			ENDING = 4
		};

	public:
		Pacman();
		void SwitchTo(int screen, ScreenData* data = nullptr) override;
		~Pacman() override;
};
