#pragma once
#include "Actor.h"
#include <vector>

class Player;
class Ghost;
class Map;

// Main manager of the pacman game,
// which aims to hold the map, the player,
// and the ghosts.
class GameManager : public Actor
{
	public:
		enum State { None, Playing, Win, Loose };

	private:
		Map* _map;
		Player* _player;
		std::vector<Ghost*> _ghosts;
		State _state;
		//
		float _sirenSoundDeltaTime;

	public:
		GameManager();
		void Update() override;
		//
		State GetState();
		int GetScore();
		int GetLifes();
		void FrightenGhosts();
		void DestroyGhosts();
		void ResetGame();
		void WinGame();
		void LooseGame();
};
