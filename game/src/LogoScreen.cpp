#include "LogoScreen.h"
#include "ResourcesManager.h"
#include "Pacman.h"

LogoScreen::LogoScreen(ScreensManager* manager, ScreenData* data) : Screen(manager)
{
	_image = ResourcesManager::LogoImage;
	_imageAspectRatio = (float)_image.height / (float)_image.width;
	_source = { 0, 0, (float)_image.width, (float)_image.height };
	_dest = { (float)(_width / 2 - _image.width / 2), (float)(_height / 4), (float)(_width / 4), (float)(_width / 4) * _imageAspectRatio};

	_time = 3;
	_progress = 0;
}

void LogoScreen::Update()
{
	_progress += GetFrameTime() / _time;

	if (_progress >= 1)
	{
		_manager->SwitchTo(Pacman::Screens::TITLE);
	}
}

// Simple effect to change the color given a certain progress (0-1)
Color ComputeColor(float progress)
{
	auto c = WHITE;
	c.a = (1 - progress) * 2 * 255;
	return c;
}

void LogoScreen::Draw()
{
	DrawTexturePro(_image, _source, _dest, { (float)(0), (float)(0) }, 0, ComputeColor(_progress));
}
