#pragma once
#include <raylib.h>

// Class holding all the resources, including Load/Unload methods.
class ResourcesManager
{
	private:
		static void LoadTextures();
		static void UnloadTextures();
		static void LoadSounds();
		static void UnloadSounds();
		static void LoadTilemaps();
		static void LoadFonts();
		static void UnloadFonts();

	public:
		// Textures
		static Texture2D LogoImage;
		static Texture2D TitleImage;
		static Texture2D PacmanImage;
		static Texture2D PacmanDeadImage;
		static Texture2D LifesImage;
		static Texture2D EnemiesImage;
		static Texture2D PacmanTilesetImage;
		// Audio
		static Sound IntroTheme;
		static Sound GameStartTheme;
		static Sound VictoryTheme;
		static Sound PacmanDeathSound;
		static Music SirenSound;
		static Sound EatingGhostSound;
		static Sound EatingPowerPillSound;
		static Music WakaWakaSound;
		static Sound GameOverTheme;
		// Tilemaps
		static const char* TilesMap;
		static const char* CollidersMap;
		static const char* ObjectsMap;
		// Fonts
		static Font PacmanFont;
		//
		static void LoadResources();
		static void UnloadResources();
};
