#pragma once
#include "Tilemap.h"

// Class that implements the calculation of the rectangles
// given the map's texture
class PacmanTilemap : public Tilemap
{
	public:
		PacmanTilemap();
		std::vector<Rectangle>
			GetRectanglesFromTexture() override;
};
