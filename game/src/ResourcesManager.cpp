#include "ResourcesManager.h"
#include <iostream>
#include <string>
#include <fstream>

// Textures

Texture2D ResourcesManager::LogoImage;
Texture2D ResourcesManager::TitleImage;
Texture2D ResourcesManager::PacmanImage;
Texture2D ResourcesManager::PacmanDeadImage;
Texture2D ResourcesManager::LifesImage;
Texture2D ResourcesManager::EnemiesImage;
Texture2D ResourcesManager::PacmanTilesetImage;

void ResourcesManager::LoadTextures()
{
	LogoImage = LoadTexture("resources/Menu/Logo.png");
	TitleImage = LoadTexture("resources/Menu/PacMan_MainLogo.png");
	PacmanImage = LoadTexture("resources/Game/PacMan.png");
	PacmanDeadImage = LoadTexture("resources/Game/PacManDead.png");
	LifesImage = LoadTexture("resources/Game/IconLifes.png");
	EnemiesImage = LoadTexture("resources/Game/Enemies.png");
	PacmanTilesetImage = LoadTexture("resources/Game/PacManTileset.png");
}

void ResourcesManager::UnloadTextures()
{
	UnloadTexture(LogoImage);
	UnloadTexture(TitleImage);
	UnloadTexture(PacmanImage);
	UnloadTexture(PacmanDeadImage);
	UnloadTexture(LifesImage);
	UnloadTexture(EnemiesImage);
	UnloadTexture(PacmanTilesetImage);
}

// Audio

Sound ResourcesManager::IntroTheme;
Sound ResourcesManager::GameStartTheme;
Sound ResourcesManager::VictoryTheme;
Sound ResourcesManager::PacmanDeathSound;
Music ResourcesManager::SirenSound;
Sound ResourcesManager::EatingGhostSound;
Sound ResourcesManager::EatingPowerPillSound;
Music ResourcesManager::WakaWakaSound;
Sound ResourcesManager::GameOverTheme;

void ResourcesManager::LoadSounds()
{
	IntroTheme = LoadSound("resources/Audio/Music/IntroTheme.mp3");
	GameStartTheme = LoadSound("resources/Audio/Sounds/GameStart.mp3");
	VictoryTheme = LoadSound("resources/Audio/Sounds/Victory.mp3");
	PacmanDeathSound = LoadSound("resources/Audio/Sounds/Death.mp3");
	SirenSound = LoadMusicStream("resources/Audio/Sounds/Siren.mp3");
	SirenSound.looping = true;
	EatingGhostSound = LoadSound("resources/Audio/Sounds/EatingGhost.mp3");
	EatingPowerPillSound = LoadSound("resources/Audio/Sounds/EatingPowerPill.mp3");
	WakaWakaSound = LoadMusicStream("resources/Audio/Sounds/WakaWaka.mp3");
	WakaWakaSound.looping = true;
	GameOverTheme = LoadSound("resources/Audio/Music/GameOverTheme.mp3");
}

void ResourcesManager::UnloadSounds()
{
	UnloadSound(IntroTheme);
	UnloadSound(GameStartTheme);
	UnloadSound(VictoryTheme);
	UnloadSound(PacmanDeathSound);
	UnloadMusicStream(SirenSound);
	UnloadSound(EatingGhostSound);
	UnloadSound(EatingPowerPillSound);
	UnloadMusicStream(WakaWakaSound);
	UnloadSound(GameOverTheme);
}

// Tilemaps

const char* ResourcesManager::TilesMap;
const char* ResourcesManager::CollidersMap;
const char* ResourcesManager::ObjectsMap;

void ResourcesManager::LoadTilemaps()
{
	TilesMap = LoadFileText("resources/Tilemaps/Tilemap.txt");
	CollidersMap = LoadFileText("resources/Tilemaps/Tilemap_Colliders.txt");
	ObjectsMap = LoadFileText("resources/Tilemaps/Tilemap_Objects.txt");
}

// Font

Font ResourcesManager::PacmanFont;

void ResourcesManager::LoadFonts()
{
	PacmanFont = LoadFont("resources/Font/PacManFont.ttf");
}

void ResourcesManager::UnloadFonts()
{
	UnloadFont(PacmanFont);
}

//

void ResourcesManager::LoadResources()
{
	LoadTextures();
	LoadSounds();
	LoadTilemaps();
}

void ResourcesManager::UnloadResources()
{
	UnloadTextures();
	UnloadSounds();
}