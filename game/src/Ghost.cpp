#include "Ghost.h"
#include "Player.h"
#include <iostream>

Ghost::Ghost(Texture2D image, Map* map, Player* player) : Actor(1)
{
	_image = image;
	//
	_map = map;
	_cellSize = _map->GetCellSize();
	_body = { 0, 0, (float)_cellSize, (float)_cellSize };
	_player = player;
	//
	_state = Ghost::State::Chase;
	//
	_sprite = 0;
	_timeBetweenSprites = 0.1f;
	_deltaTimeBetweenSprites = 0;
	//
	_frightenedTime = 10;
	_frightenedColor = 0;
	_frightenedColorTime = 0;
	_deltaFrightenedTime = 0;
	//
	_lastPosition = { 0, 0 };
	//_tileProgress = 0;
	_speed = 3.0f; //tiles per second
	//_speed = 0.1f;
	_tileProgress = 1;
	_targetDirection = Map::Right;
}

void Ghost::Start()
{
	InitChaseSprites(_image, _chaseSprites);
	InitFrightenedSprites(_image, _frightenedSprites);
	InitEatenSprites(_image, _eatenSprites);
	//
	auto p = GetStartPosition();
	_targetPosition = p;
	_body.x = p.x;
	_body.y = p.y;
}

void Ghost::Update()
{
	// When _tileProgress >= 1, the ghost has reached the next tile,
	// so it must decide the next one depending on the state.
	if (_tileProgress >= 1)
	{
		_lastPosition = _targetPosition;
		map<Map::Direction, Vector2> adjacents;

		switch (_state)
		{
			case Ghost::Chase:
				adjacents = _map->GetAvailableAdjacent(_targetPosition, _targetDirection == Map::Stop);
				_targetDirection = ComputeNextDirectionChase(adjacents, _targetDirection);
				break;

			case Ghost::Frightened:
				adjacents = _map->GetAvailableAdjacent(_targetPosition);
				_targetDirection = ComputeNextDirectionFrightened(adjacents, _targetDirection);
				break;

			case Ghost::Eaten:
				adjacents = _map->GetAvailableAdjacent(_targetPosition, true);
				_targetDirection = ComputeNextDirectionEaten(adjacents, _targetDirection);
				break;
		}

		_targetPosition = adjacents[_targetDirection];
		_tileProgress = 0;
	}

	// If the state is Frightened, it must check constantly until
	// the time finishes.
	if (_state == Ghost::Frightened)
	{
		_deltaFrightenedTime -= GetFrameTime();
		if (_deltaFrightenedTime <= 0)
		{
			ChangeState(Ghost::Chase);
		}
		else if (_deltaFrightenedTime <= _frightenedColorTime)
		{
			// To make the blink effect a few times right before
			// the frightened time finishes.
			_frightenedColorTime -= 0.5f;
			int size = _frightenedSprites.size();
			_frightenedColor = (_frightenedColor + size / 2) % size;
		}
	}

	// Lerp from last position to target position.
	_body.x = _lastPosition.x + (_targetPosition.x - _lastPosition.x) * _tileProgress;
	_body.y = _lastPosition.y + (_targetPosition.y - _lastPosition.y) * _tileProgress;

	_tileProgress += _speed * GetFrameTime();
}

void Ghost::Draw()
{
	_deltaTimeBetweenSprites -= GetFrameTime();
	if (_deltaTimeBetweenSprites <= 0)
	{
		_deltaTimeBetweenSprites = _timeBetweenSprites;
		_sprite++;
	}

	Rectangle sprite;
	switch (_state)
	{
		case Ghost::State::Chase:
			auto chase = _chaseSprites[_targetDirection];
			sprite = chase[_sprite % chase.size()];
			break;

		case Ghost::State::Frightened:
			auto frightened = _frightenedSprites;
			sprite = frightened[_sprite % (frightened.size() / 2) + _frightenedColor];
			break;

		case Ghost::State::Eaten:
			sprite = _eatenSprites[_targetDirection];
			break;
	}

	DrawTexturePro(_image,
		sprite,
		{ _body.x, _body.y, (float)_cellSize, (float)_cellSize},
		{ 0, 0 },
		0,
		WHITE
	);
}

Ghost::State Ghost::GetState()
{
	return _state;
}

void Ghost::ChangeState(State newState)
{
	switch (newState)
	{
		case Ghost::State::Chase:
			//_lastDirection = _targetDirection = Map::Stop;
			break;

		case Ghost::State::Frightened:
			_deltaFrightenedTime = _frightenedTime;
			_frightenedColorTime = _frightenedTime * 0.3f;
			_sprite = 0;
			break;

		case Ghost::State::Eaten:
			break;
	}

	_state = newState;
}

Rectangle Ghost::GetBody()
{
	return _body;
}
