#pragma once
#include "Actor.h"
#include <map>
#include <memory>
#include <array>
#include "Map.h"

class Player;

using namespace std;

// Base class for any ghost
class Ghost : public Actor
{
	public:
		enum State { Chase, Frightened, Eaten };

	private:
		// Texture containing all the sprites of any ghost
		Texture2D _image;
		// Chase state sprites are stored as 2 sprites per
		// each direction (animation)
		map<Map::Direction, array<Rectangle, 2>> _chaseSprites;
		// Frightened state sprites are stored as an array
		// of 4 because it contains the blue frightened sprites
		// (first 2) and the white frightened sprites (second 2)
		array<Rectangle, 4> _frightenedSprites;
		// Eaten state sprites are stored as 1 sprite per 
		// each direction
		map<Map::Direction, Rectangle> _eatenSprites;
		//
		float _timeBetweenSprites;
		float _deltaTimeBetweenSprites;
		int _sprite;
		//
		float _frightenedTime;
		float _deltaFrightenedTime;
		int _frightenedColor;
		float _frightenedColorTime;
		//
		float _tileProgress;
		float _speed;
		Vector2 _position;
		Vector2 _lastPosition;
		Vector2 _targetPosition;
		Map::Direction _targetDirection;
		Map::Direction _lastDirection;

	protected:
		// InitXXX methods are pure virtual so the derived class
		// initializes the rectangles because is the one that
		// knows where (in the main texture).
		virtual void InitChaseSprites(Texture2D, map<Map::Direction, array<Rectangle, 2>>&) = 0;
		virtual void InitFrightenedSprites(Texture2D, array<Rectangle, 4>&) = 0;
		virtual void InitEatenSprites(Texture2D, map<Map::Direction, Rectangle>&) = 0;
		// ComputeNextDirectionXXX methods are called when the ghost
		// reaches the next tile, and they are pure virtual so the
		// derived class can implement them (ghosts have different
		// behaviours).
		virtual Map::Direction ComputeNextDirectionChase(map<Map::Direction, Vector2>, Map::Direction) = 0;
		virtual Map::Direction ComputeNextDirectionFrightened(map<Map::Direction, Vector2>, Map::Direction) = 0;
		virtual Map::Direction ComputeNextDirectionEaten(map<Map::Direction, Vector2>, Map::Direction) = 0;
		// Get position methods are pure virtual so the derived class
		// can determine the different positions of the ghost.
		virtual Vector2 GetStartPosition() = 0;
		virtual Vector2 GetFrightenedPosition() = 0;
		virtual Vector2 GetEatenPosition() = 0;
		//
		Rectangle _spriteToDraw;
		//
		Rectangle _body;
		//
		Map* _map;
		int _cellSize;
		Player* _player;
		State _state;

	public:
		Ghost(Texture2D image, Map* _map, Player* player);
		void Start() override;
		void Update() override;
		void Draw() override;
		Rectangle GetBody() override;
		State GetState();
		virtual void ChangeState(State);
};
