#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "ScreenData.h"

// Screen containing the main information about the game.
class OptionsScreen : public Screen
{
	private:
		const char* _text;
		Vector2 _textPosition;

	public:
		OptionsScreen(ScreensManager*, ScreenData* = nullptr);
		void Update() override;
		void Draw() override;
};
