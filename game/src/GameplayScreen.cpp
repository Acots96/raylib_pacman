#include "GameplayScreen.h"
#include <string>
#include "Pacman.h"
#include "ResourcesManager.h"
#include "EndingScreenData.h"

GameplayScreen::GameplayScreen(ScreensManager* manager, ScreenData* data) : Screen(manager)
{
	Engine::ResetElapsedTime();
	_game = new GameManager();
	Engine::Instantiate(_game);
}

void GameplayScreen::Update()
{
	if (_game->GetState() != GameManager::State::Playing)
	{
		_manager->SwitchTo(Pacman::Screens::ENDING, 
			new EndingScreenData(_game->GetState() == GameManager::Win));
	}
}

void GameplayScreen::Draw()
{
	float w = _width;
	float h = _height;
	float y = h * 0.02f;

	int fontSize = w / 40;
	float spacing = 2.0f;
	Font font = ResourcesManager::PacmanFont;

	// Score
	int len = MeasureText("SCORE: ", fontSize);
	int posX = w * 0.1f;
	int incr = w * 0.02f;
	DrawTextEx(font, "SCORE: ", { (float)posX, y }, fontSize, spacing, WHITE);
	string s = to_string(_game->GetScore());
	DrawTextEx(font, s.c_str(), { (float)posX + len + incr, y }, fontSize, spacing, WHITE);

	// Lifes
	posX = w * 0.8f;
	int size = h * 0.05f;
	int lifes = _game->GetLifes() - 1;
	for (int i = 0; i < lifes; i++)
	{
		Texture2D t = ResourcesManager::LifesImage;
		DrawTexturePro(t,
			{ 0, 0, (float)t.width, (float)t.height },
			{ (float)(posX + i * size), y, (float)size, (float)size },
			{ 0, 0 },
			0,
			WHITE
		);
	}
}

GameplayScreen::~GameplayScreen()
{
	Engine::Destroy(_game);
}
