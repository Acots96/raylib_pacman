#include "Player.h"
#include "ResourcesManager.h"
#include <iostream>
#include "Ghost.h"
#include "GameManager.h"

Player::Player(Map* map, GameManager* manager) : Actor(1)
{
	_player = ResourcesManager::PacmanImage;
	_playerDead = ResourcesManager::PacmanDeadImage;
	_map = map;
	_manager = manager;
	_cellSize = _map->GetCellSize();
	_position = map->TileToMapPosition(17, 13);
	_body = { _position.x, _position.y, (float)_cellSize, (float)_cellSize };
	_speed = 2; //Pixels per frame
	_score = _pillsEaten = 0;

	// Load all the sprites rectangles, one per each direction plus dead ones.
	int rows = 4;
	int cols = 2;
	int rowCellSize = _player.height / rows;
	int colCellSize = _player.width / cols;
	_rectangles.reserve(rows + 1);
	for (int y = 0; y <= 3; y++)
	{
		vector<Rectangle> vr;
		vr.reserve(cols);
		for (int x = 0; x <= 1; x++)
		{
			Rectangle r = { x * colCellSize, y * rowCellSize, colCellSize, rowCellSize };
			vr.push_back(r);
		}
		_rectangles.push_back(vr);
	}
	// Load all the sprites rectangles for dead animation
	vector<Rectangle> dead;
	dead.reserve(12);
	for (int x = 0; x <= 12; x++)
	{
		Rectangle r = { x * colCellSize, 0, colCellSize, rowCellSize };
		dead.push_back(r);
	}
	_rectangles.push_back(dead);

	_sprite = 0;
	_direction = Map::Direction::Right;
	_timeBetweenSprites = 0.1f;

	_doDieEffect = false;
	_waitToStartDeath = 1;
	_timePerDeadSprite = 0.2f;
	_deltaDeathTime = 0;
	_deltaTimeBetweenSprites = 0;
	_lifes = 3;

	_isWakaWakaPlaying = false;
}

void Player::Reset()
{
	_doDieEffect = false;
	_position = _map->TileToMapPosition(17, 13);
	_body = { _position.x, _position.y, (float)_cellSize, (float)_cellSize };
	_sprite = 0;
	_isWakaWakaPlaying = false;
}

void Player::Update()
{
	// If the player is dead has to do the animation before the reset
	if (_doDieEffect)
	{
		_deltaDeathTime -= GetFrameTime();
		// Next dead sprite
		if (_deltaDeathTime <= 0)
		{
			// Last sprite reached, so reset the game if Pacman has lifes,
			// otherwise loose the game.
			if (_sprite == _rectangles[_rectangles.size() - 1].size() - 1)
			{
				if (_lifes <= 0) _manager->LooseGame();
				else _manager->ResetGame();
				return;
			}
			_sprite++;
			_deltaDeathTime = _timePerDeadSprite;
		}
		return;
	}

	if (_pillsEaten >= _map->GetPillsAmount())
	{
		_manager->WinGame();
	}

	if (IsKeyDown(KEY_D) || IsKeyDown(KEY_RIGHT)) _direction = Map::Direction::Right;
	else if (IsKeyDown(KEY_A) || IsKeyDown(KEY_LEFT)) _direction = Map::Direction::Left;
	else if (IsKeyDown(KEY_W) || IsKeyDown(KEY_UP)) _direction = Map::Direction::Up;
	else if (IsKeyDown(KEY_S) || IsKeyDown(KEY_DOWN)) _direction = Map::Direction::Down;

	Vector2 dir = _map->ComputeDirection(_direction);

	Vector2 nextPosition = {
		(int)(_position.x + dir.x * _speed + 0.5f),
		(int)(_position.y + dir.y * _speed + 0.5f),
	};

	nextPosition = _map->ComputeNextPosition(_position, nextPosition);

	// Animate the character only if it is moving.
	if (_position.x != nextPosition.x || _position.y != nextPosition.y)
	{
		_position.x = nextPosition.x;
		_position.y = nextPosition.y;
		_body.x = _position.x;
		_body.y = _position.y;

		_deltaTimeBetweenSprites -= GetFrameTime();
		if (_deltaTimeBetweenSprites <= 0)
		{
			_deltaTimeBetweenSprites = _timeBetweenSprites;
			_sprite++;
		}

		if (!_isWakaWakaPlaying)
		{
			_isWakaWakaPlaying = true;
			PlayMusicStream(ResourcesManager::WakaWakaSound);
		}
	}
	else
	{
		if (_isWakaWakaPlaying)
		{
			_isWakaWakaPlaying = false;
			StopMusicStream(ResourcesManager::WakaWakaSound);
		}
	}

	UpdateMusicStream(ResourcesManager::WakaWakaSound);

	switch (_map->CheckObject(_position))
	{
		case Map::Pill: _score += 10; _pillsEaten++;
			cout << _pillsEaten << "/" << _map->GetPillsAmount() << "\n";
			break;

		case Map::Bigpill: _score += 50;
			_pillsEaten++;
			_manager->FrightenGhosts();
			SetSoundVolume(ResourcesManager::EatingPowerPillSound, 0.7f);
			PlaySound(ResourcesManager::EatingPowerPillSound);
			cout << _pillsEaten << "/" << _map->GetPillsAmount() << "\n";
			break;
	}
}

void Player::Draw()
{
	Rectangle dest = { _position.x, _position.y, _cellSize, _cellSize };
	int idx = _doDieEffect ? _rectangles.size() - 1 : _direction;
	auto sprites = _rectangles[idx];

	DrawTexturePro(
		_doDieEffect ? _playerDead : _player,
		sprites[_sprite % sprites.size()],
		dest,
		{ 0, 0 },
		0,
		WHITE
	);
}

int Player::GetScore()
{
	return _score;
}
int Player::GetLifes()
{
	return _lifes;
}

Vector2 Player::GetPosition()
{
	return _position;
}

Rectangle Player::GetBody()
{
	return _body;
}

void Player::CollisionWith(Actor* actor)
{
	if (auto g = dynamic_cast<Ghost*>(actor))
	{
		switch (g->GetState())
		{
			// Ghost is in Chase mode -> Pacman dies
			case Ghost::Chase:
				Die();
				break;

			// Ghost is frightened -> Ghost dies
			case Ghost::Frightened:
				_score += 200;
				PlaySound(ResourcesManager::EatingGhostSound);
				g->ChangeState(Ghost::Eaten);
				break;
		}
	}
}

void Player::Die()
{
	if (_doDieEffect) return;

	PlaySound(ResourcesManager::PacmanDeathSound);
	_lifes--;
	_manager->DestroyGhosts();
	_doDieEffect = true;
	_deltaDeathTime = _waitToStartDeath;
	_sprite = 0;
}

Player::~Player()
{
	_map = nullptr;
	_manager = nullptr;
}
