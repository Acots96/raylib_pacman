#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "GameManager.h"
#include "ScreenData.h"

// Screen shown when the game is running.
// Creates the GameManager and draws the main information
// of the game in the top of it.
class GameplayScreen : public Screen
{
	private:
		GameManager* _game;

	public:
		GameplayScreen(ScreensManager*, ScreenData* = nullptr);
		void Update() override;
		void Draw() override;
		~GameplayScreen() override;
};
