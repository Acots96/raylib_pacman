#include "Map.h"
#include "ResourcesManager.h"
#include "Tilemap.h"
#include "GameManager.h"
#include <iostream>

// With the given TilemapData, fills the given data structure with the tunnels info.
// The tunnels parameter is a map [KEY,VALUE] whose KEY is the number representing
// the tunnel in the TilemapData, and the VALUE is a vector of vectors where each
// subvector is: tunnel entrance tile position, tunnel exit tile position.
void GenerateTunnels(Tilemap::TilemapData colliders, map<int, vector<vector<int>>>& tunnels)
{
	for (int y = 0; y < colliders.size(); y++)
	{
		auto row = colliders[y];
		for (int x = 0; x < row.size(); x++)
		{
			// tile number > 1 menas tunnel
			if (row[x] > 1)
			{
				if (tunnels.find(row[x]) == tunnels.end())
				{
					tunnels[row[x]].reserve(2);
				}

				// Now finds the first available adjacent tile (walkable)
				// to the tunnel tile, which will be the tunnel's exit.

				vector<int> entranceExit;
				entranceExit.reserve(2);
				// left
				int y2 = y;
				int x2 = max(x - 1, 0);
				if (colliders[y2][x2] == 0)
				{
					entranceExit.push_back(y * row.size() + x);
					entranceExit.push_back(y2 * row.size() + x2);
					tunnels[row[x]].push_back(entranceExit);
					continue;
				}
				// right
				y2 = y;
				x2 = min(x + 1, (int)row.size() - 1);
				if (colliders[y2][x2] == 0)
				{
					entranceExit.push_back(y * row.size() + x);
					entranceExit.push_back(y2 * row.size() + x2);
					tunnels[row[x]].push_back(entranceExit);
					continue;
				}
				// up
				y2 = max(y - 1, 0);
				x2 = x;
				if (colliders[y2][x2] == 0)
				{
					entranceExit.push_back(y * row.size() + x);
					entranceExit.push_back(y2 * row.size() + x2);
					tunnels[row[x]].push_back(entranceExit);
					continue;
				}
				// down
				y2 = min(y + 1, (int)colliders.size() - 1);
				x2 = x;
				if (colliders[y2][x2] == 0)
				{
					entranceExit.push_back(y * row.size() + x);
					entranceExit.push_back(y2 * row.size() + x2);
					tunnels[row[x]].push_back(entranceExit);
					continue;
				}
			}
		}
	}
}

Map::Map(Vector2 startPos, int cellSize, Tilemap* tilemap, GameManager* manager) : Actor(0)
{
	_tilemap = tilemap;
	_manager = manager;

	_start = startPos;
	_cellSize = cellSize;
	_rows = _tilemap->map.size();
	_columns = _tilemap->map[0].size();
	_screenRect = { 
		_start.x,
		_start.y,
		_start.x + _columns * _cellSize,
		_start.y + _rows * _cellSize
	};

	_rectangles = _tilemap->GetRectanglesFromTexture();

	// All map tiles values
	_tiles.reserve(_rows * _columns);
	for (int y = 0; y < _tilemap->map.size(); y++)
	{
		auto row = _tilemap->map[y];
		for (int x = 0; x < row.size(); x++)
		{
			_tiles.push_back(row[x]);
		}
	}

	// All map objects values
	_pillsAmount = 0;
	_objects.reserve(_rows * _columns);
	for (int y = 0; y < _tilemap->objects.size(); y++)
	{
		auto row = _tilemap->objects[y];
		for (int x = 0; x < row.size(); x++)
		{
			int cell = row[x];
			if (cell == Pill || cell == Bigpill) _pillsAmount++;
			_objects.push_back(row[x]);
		}
	}

	// Finds the tunnels with the given TilemapData to store it and use it later
	map<int, vector<vector<int>>> tunnels;
	GenerateTunnels(_tilemap->colliders, tunnels);	
	for (auto it = tunnels.begin(); it != tunnels.end(); ++it)
	{
		auto tunnel = it->second;
		auto entrance1 = tunnel.at(0);
		auto entrance2 = tunnel.at(1);
		_tunnels[entrance1.front()] = entrance2.back();
		_tunnels[entrance2.front()] = entrance1.back();
	}
}

float Map::Distance(Vector2 p1, Vector2 p2)
{
	return sqrtf(powf(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

Vector2 Map::TileToMapPosition(int row, int col)
{
	return Vector2{
		_start.x + col * _cellSize,
		_start.y + row * _cellSize
	};
}

Vector2 Map::ComputeDirection(Map::Direction dir)
{
	switch (dir)
	{
		case Map::Direction::Right: return { 1, 0 };
		case Map::Direction::Left: return { -1, 0 };
		case Map::Direction::Up: return { 0, -1 };
		case Map::Direction::Down: return { 0, 1 };
	}
}

map<Map::Direction, Vector2> Map::GetAvailableAdjacent(Vector2 position, bool includeNegative)
{
	// Get the center of the tile map
	int offset = _cellSize * 0.5f;
	Vector2 center = {
		position.x + offset,
		position.y + offset
	};

	// Get all 4 adjacent tiles
	map<Map::Direction, Vector2> adjacent = {
		{ Right, Vector2 { center.x + _cellSize, center.y } },
		{ Left, Vector2 { center.x - _cellSize, center.y } },
		{ Up, Vector2 { center.x, center.y - _cellSize } },
		{ Down, Vector2 { center.x, center.y + _cellSize } },
	};

	// Check whether they are reachable or not
	auto it = adjacent.begin();
	while (it != adjacent.end())
	{
		int row = (it->second.y - _start.y) / _cellSize;
		int col = (it->second.x - _start.x) / _cellSize;
		int cell = _tilemap->colliders[row][col];

		// Reachable
		if (cell == 0 || includeNegative && cell < 0)
		{
			// Get the real tile position
			it->second.x -= offset;
			it->second.y -= offset;
			it++;
		}
		// Not reachable, remove
		else it = adjacent.erase(it);
	}

	return adjacent;
}

Map::Direction Map::GetOppositeDirection(Map::Direction dir)
{
	switch (dir)
	{
		case Right: return Left;
		case Left: return Right;
		case Up: return Down;
		case Down: return Up;
	}
}

Vector2 Map::ComputeNextPosition(Vector2 position, Vector2 nextPosition)
{
	// Get all possible adjacents center
	int offset = _cellSize * 0.2f;
	array<Vector2, 4> corners = {
		Vector2 { nextPosition.x + offset, nextPosition.y + offset },
 		Vector2 { nextPosition.x + offset, nextPosition.y + _cellSize - offset },
		Vector2 { nextPosition.x + _cellSize - offset, nextPosition.y + offset },
		Vector2 { nextPosition.x + _cellSize - offset, nextPosition.y + _cellSize - offset },
	};

	auto colliders = _tilemap->colliders;

	// Colliders and tunnels
	for (Vector2 c : corners)
	{
		int row = (c.y - _start.y) / _cellSize;
		int col = (c.x - _start.x) / _cellSize;

		int cell = colliders[row][col];
		if (cell > 0)
		{
			// Tunnel
			if (cell == 2)
			{
				int idx = _tunnels[row * colliders[0].size() + col];
				col = idx % colliders[0].size();
				row = (idx - col) / colliders[0].size();

				Vector2 tilemapPos = { _start.x + col * _cellSize, _start.y + row * _cellSize };
				return tilemapPos;
			}
			return position;
		}
	}

	return nextPosition;
}

Map::ObjectType Map::CheckObject(Vector2 position, bool removeIfConsumible)
{
	// Get all possible adjacent tiles
	int offset = _cellSize * 0.5f;
	array<Vector2, 4> corners = {
		Vector2 { position.x + offset, position.y + offset },
		Vector2 { position.x + offset, position.y + _cellSize - offset },
		Vector2 { position.x + _cellSize - offset, position.y + offset },
		Vector2 { position.x + _cellSize - offset, position.y + _cellSize - offset },
	};

	for (Vector2 c : corners)
	{
		int row = (c.y - _start.y) / _cellSize;
		int col = (c.x - _start.x) / _cellSize;

		int cell = _tilemap->objects[row][col];
		if (cell > 0)
		{
			if (removeIfConsumible)
			{
				_tilemap->objects[row][col] = None;
				_objects[row * _tilemap->colliders[0].size() + col] = None;
			}
			return (Map::ObjectType)cell;
		}
	}

	return None;
}

int Map::GetCellSize()
{
	return _cellSize;
}

int Map::GetPillsAmount()
{
	return _pillsAmount;
}

void Map::Draw()
{
	int tile = 0;
	for (float y = _screenRect.y; y < _screenRect.height; y += _cellSize)
	{
		for (float x = _screenRect.x; x < _screenRect.width; x += _cellSize)
		{
			Rectangle dest = { x, y, _cellSize, _cellSize };
			// tile
			DrawTexturePro(
				_tilemap->tilesSamples,
				_rectangles[_tiles[tile]],
				dest,
				{ 0, 0 },
				0,
				WHITE
			);
			// pill
			DrawTexturePro(
				_tilemap->tilesSamples,
				_rectangles[_objects[tile]],
				dest,
				{ 0, 0 },
				0,
				WHITE
			);
			tile++;
		}
	}
}
