#include "GameManager.h"
#include "Map.h"
#include "Player.h"
#include "BlinkyGhost.h"
#include "PacmanTilemap.h"
#include "ResourcesManager.h"

GameManager::GameManager()
{
	int screenW = GetRenderWidth();
	int mapW = screenW * 0.8f;
	int cell = mapW / 27;
	Vector2 startMapPos = { screenW * 0.1f, GetRenderHeight() * 0.1f};

	Tilemap* tm = new PacmanTilemap();
	_map = new Map(startMapPos, cell, tm, this);
	_player = new Player(_map, this);

	Engine::Instantiate(_map);
	Engine::Instantiate(_player);
	
	ResetGame();

	PlaySound(ResourcesManager::GameStartTheme);
	_sirenSoundDeltaTime = 4; // GameStartTheme length
	PlayMusicStream(ResourcesManager::SirenSound);
}

void GameManager::Update()
{
	if (_sirenSoundDeltaTime > 0)
	{
		_sirenSoundDeltaTime -= GetFrameTime();
	}
	else
	{
		UpdateMusicStream(ResourcesManager::SirenSound);
	}
}

void GameManager::FrightenGhosts()
{
	for (auto g : _ghosts) g->ChangeState(Ghost::State::Frightened);
	_sirenSoundDeltaTime = 10; // Frightened state time
}
void GameManager::DestroyGhosts()
{
	for (auto g : _ghosts) Engine::Destroy(g);
	_ghosts.clear();
	_sirenSoundDeltaTime = 3.5; // Pacman death animation time (more or less)
}

GameManager::State GameManager::GetState()
{
	return _state;
}

int GameManager::GetScore()
{
	return _player->GetScore();
}
int GameManager::GetLifes()
{
	return _player->GetLifes();
}

void GameManager::ResetGame()
{
	_player->Reset();
	_ghosts.push_back(new BlinkyGhost(_map, _player));
	for (auto g : _ghosts) Engine::Instantiate(g);

	_state = Playing;
}

void GameManager::WinGame()
{
	DestroyGhosts();
	Engine::Destroy(_player);
	Engine::Destroy(_map);
	StopMusicStream(ResourcesManager::SirenSound);

	_state = Win;
}

void GameManager::LooseGame()
{
	DestroyGhosts();
	Engine::Destroy(_player);
	Engine::Destroy(_map);
	StopMusicStream(ResourcesManager::SirenSound);

	_state = Loose;
}
