#include "OptionsScreen.h"
#include "Pacman.h"

OptionsScreen::OptionsScreen(ScreensManager* manager, ScreenData* data) : Screen(manager)
{
	_text = "To move PACMAN use the direction keys or A,D,W,S.\n"
		"PACMAN has 3 lifes, every dot increases e10 points\n"
		"the big dots adds 50 points to the score.\n"
		"To win PACMAN has to eat all the dots\n"
		"You loose if the ghost SHADOW kills you 3 times.\n\n\n"
		"Press 'O' to return to Title.";

	int textWidth = MeasureText(_text, _width / 45);
	_textPosition = { (float)(_width / 2 - textWidth / 2), _height * 0.1f };
}

void OptionsScreen::Update()
{
	if (IsKeyReleased(KEY_O))
	{
		_manager->SwitchTo(Pacman::Screens::TITLE);
	}
}

void OptionsScreen::Draw()
{
	DrawText(_text, _textPosition.x, _textPosition.y, _width / 45, WHITE);
}
