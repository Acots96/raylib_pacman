#include "Actor.h"
#include "Engine.h"
#include <iostream>

using namespace std;

Engine* Engine::_instance;
double Engine::_elapsedTime;

void Engine::Start(int screenWidth, int screenHeight, int targetFPS, const char* title)
{
	// Initialize Raylib.
	InitWindow(screenWidth, screenHeight, title);
	SetTargetFPS(targetFPS);
	InitAudioDevice();

	// Create the instance.
	if (_instance != nullptr)
		delete _instance;
	_instance = new Engine();
}
Engine::Engine()
{
	_screen = { 0, 0, (float)GetRenderWidth(), (float)GetRenderHeight() };
	_elapsedTime = 0;
}

void Engine::Execute()
{
	while (!WindowShouldClose())
	{
		_instance->Update();
		_elapsedTime += GetFrameTime();
	}

	delete _instance;

	// Obsolete
	//StopSoundMulti();
	CloseAudioDevice();
	CloseWindow();
}

Engine::~Engine()
{
	for (auto it = _actors.begin(); it != _actors.end(); ++it)
		it->clear();
	_actors.clear();
}

void Engine::Instantiate(Actor* actor)
{
	_instance->AddActor(actor);
}

// Check amongst all the existing actors before adding a 
// new one so it will not be added twice or more.
void Engine::AddActor(Actor* actor)
{
	for (auto layerList = _actors.begin(); layerList != _actors.end(); ++layerList)
	{
		// For each sublist checks the layer of the first actor.
		auto subList = layerList->begin();
		std::advance(subList, 0);
		
		// Add the new actor to this sublist if new actor's layer
		// and sublist's layer are equal.
		if (subList->actor->GetLayer() == actor->GetLayer())
		{
			for (auto state = layerList->begin(); state != layerList->end(); ++state)
				if (state->actor == actor) return;

			layerList->push_back(ActorState(actor, true));
			return;
		}
		// If new actor's layer is bigger than sublist's layer,
		// then this is the first actor with this layer, so a
		// new sublist must be added to the general list.
		else if (subList->actor->GetLayer() > actor->GetLayer())
		{
			list<ActorState> newLayerList;
			newLayerList.push_back(ActorState(actor, true));
			_actors.insert(layerList, newLayerList);
			return;
		}
	}

	// First layer will fall here.
	list<ActorState> newLayerList;
	newLayerList.push_back(ActorState(actor, true));
	_actors.push_back(newLayerList);
}

void Engine::Destroy(Actor* actor)
{
	_instance->RemoveActor(actor);
}

// An actor is not directly removed when Engine::DestroyActor(actor)
// is called, instead is marked as "dead" and it will be remove from 
// the list later with all the other dead actors at the same time.
void Engine::RemoveActor(Actor* actor)
{
	for (auto layerList = _actors.begin(); layerList != _actors.end(); ++layerList)
	{
		auto subList = layerList->begin();
		std::advance(subList, 0);

		if (subList->actor->GetLayer() == actor->GetLayer())
		{
			for (auto state = layerList->begin(); state != layerList->end(); ++state)
			{
				if (state->actor == actor)
				{
					state->isAlive = false;
					break;
				}
			}
		}
	}
}

// Calls the Start/Update method of all the actors.
// Computes all the collisions between.
// Removes and deletes all the actors marked as "dead".
// Calls the Draw method of all the actors.
void Engine::Update()
{
	for (auto layerList = _actors.begin(); layerList != _actors.end(); ++layerList)
	{
		for (auto state = layerList->begin(); state != layerList->end(); ++state)
		{
			if (!state->isAlive) continue;

			auto actor = state->actor;
			if (actor->IsActive)
			{
				// If this is the first time of the actor -> Start method called.
				// From the second time until they die -> Update method called.
				if (!state->isStarted)
				{
					state->isStarted = true;
					actor->Start();
				}
				else actor->Update();
			}
		}
	}

	CheckCollisions();

	RemoveDisabled();

	//

	BeginDrawing();
	ClearBackground(BLACK);

	for (auto layerList = _actors.begin(); layerList != _actors.end(); ++layerList)
	{
		for (auto state = layerList->begin(); state != layerList->end(); ++state)
		{
			if (!state->isAlive || !state->isStarted) continue;

			auto actor = state->actor;
			if (actor->IsActive)
			{
				state->actor->Draw();
			}
		}
	}

	EndDrawing();
}

double Engine::GetElapsedTime()
{
	return _elapsedTime;
}
void Engine::ResetElapsedTime()
{
	_elapsedTime = 0;
}

// Iterates over all the actors and removes the ones marked as "dead".
void Engine::RemoveDisabled()
{
	auto layerList = _actors.begin();
	while (layerList != _actors.end())
	{
		auto actor = layerList->begin();
		while (actor != layerList->end())
		{
			auto a = actor;
			if (a->actor == nullptr || !a->isAlive)
			{
				delete a->actor;
				actor = layerList->erase(actor);
			}
			else actor++;
		}

		if (layerList->size() == 0)
		{
			layerList = _actors.erase(layerList);
		}
		else layerList++;
	}
}

bool IsBodyEmpty(Rectangle r)
{
	return r.width <= 0 || r.height <= 0;
}

// Iterates over all the actors and checks if they are colliding
// with each other.
// The only collision avoided is one actor with itself.
void Engine::CheckCollisions()
{
	for (auto layerList1 = _actors.begin(); layerList1 != _actors.end(); ++layerList1)
	{
		for (auto a1 = layerList1->begin(); a1 != layerList1->end(); ++a1)
		{
			// Check if actor1 collides with any other actor.
			if (!a1->isStarted || !a1->isAlive || !a1->actor->IsActive ||
				IsBodyEmpty(a1->actor->GetBody())) 
				continue;

			auto r1 = a1->actor->GetBody();

			for (auto layerList2 = _actors.begin(); layerList2 != _actors.end(); ++layerList2)
			{
				for (auto a2 = layerList2->begin(); a2 != layerList2->end(); ++a2)
				{
					if (a1->actor == a2->actor)
						continue;

					if (!a2->isStarted || !a2->isAlive || !a2->actor->IsActive ||
						IsBodyEmpty(a2->actor->GetBody()))
						continue;

					if (CheckCollisionRecs(r1, a2->actor->GetBody()))
					{
						// If actor1 collides with actor2, notifies actor1
						a1->actor->CollisionWith(a2->actor);
					}
				}
			}
		}
	}
}
