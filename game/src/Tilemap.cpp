#include "Tilemap.h"
#include <string>
#include <iostream>

typedef Tilemap::TilemapData TMData;

Tilemap::Tilemap(Texture2D tilesSamples, const char* map,
	const char* colliders, const char* objects)
{
	this->tilesSamples = tilesSamples;
	this->map = LoadTextToTilemap(map);
	this->colliders = LoadTextToTilemap(colliders);
	this->objects = LoadTextToTilemap(objects);
}

// Reads all the text char by char, expecting only numbers and '-'
// for negatives (everything else will be treated as separator).
// Creates a string and adds all the chars until finds a non-digit char
// (excluding '-') such a comma, then parses it to a number and adds it
// to the tilemap data.
Tilemap::TilemapData Tilemap::LoadTextToTilemap(const char* text)
{
	TMData tm;
	vector<int> aux;

	string numStr = "";
	for (char c = *text; c; c = *++text)
	{
		cout << c;
		// If char is a number, or
		// if char is '-' (and is the first char in the string),
		// then adds it to the string.
		if (isdigit(c) || numStr.size() == 0 && c == '-')
		{
			numStr += c;
		}
		// Otherwise, it is a separator.
		else
		{
			// If it is not empty can be parsed to int and added.
			if (!numStr.empty())
			{
				aux.push_back(stoi(numStr));
				numStr.clear();
			}
			// The char can also be a new line, so the current row is
			// finished, added to the main vector, and ready to start
			// with the next row.
			if (c == '\n')
			{
				auto v = aux;
				tm.push_back(v);
				aux.clear();
			}
		}
	}

	// Last row: if it is not empty -> added to the main vector.
	if (!numStr.empty())
	{
		aux.push_back(stoi(numStr));
		numStr.clear();
		auto v = aux;
		tm.push_back(v);
		aux.clear();
	}

	cout << "\n";
	cout << tm.size() << "\n";
	cout << tm[0].size() << "\n";

	return tm;
}

Tilemap::~Tilemap()
{
	map.clear();
	colliders.clear();
	objects.clear();
}
