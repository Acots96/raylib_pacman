#pragma once
#include <raylib.h>
#include <vector>
#include <memory>

using namespace std;

// Base class that must implement any Tilemap.
// Loads the data of a tilemap given a texture and stores it
// as a 3 TilemapData objects (the map itself, the colliders
// and the objects).
class Tilemap
{
	public:
		// A vector of vectors, simulating the tilemap data
		typedef vector<vector<int>> TilemapData;

	public:
		Texture2D tilesSamples;
		TilemapData map;
		TilemapData colliders;
		TilemapData objects;
		//
		Tilemap(Texture2D tilesSamples, const char* map,
			const char* colliders, const char* objects);
		// Returns all the rectangles representing the sprites
		// in the texture
		virtual std::vector<Rectangle> 
			GetRectanglesFromTexture() = 0;
		virtual TilemapData LoadTextToTilemap(const char*);
		virtual ~Tilemap();
};
