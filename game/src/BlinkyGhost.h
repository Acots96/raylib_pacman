#pragma once
#include "Ghost.h"

class Map;
class Player;

// Class to represent the behaviour of the Blinky ghost.
class BlinkyGhost : public Ghost
{
	private:
		Vector2 _frightenedTargetPosition;
		Vector2 _eatenTargetPosition;

	protected:
		void InitChaseSprites(Texture2D, map<Map::Direction, array<Rectangle, 2>>&) override;
		void InitFrightenedSprites(Texture2D, array<Rectangle, 4>&) override;
		void InitEatenSprites(Texture2D, map<Map::Direction, Rectangle>&) override;
		//
		Map::Direction ComputeNextDirectionChase(map<Map::Direction, Vector2>, Map::Direction) override;
		Map::Direction ComputeNextDirectionFrightened(map<Map::Direction, Vector2>, Map::Direction) override;
		Map::Direction ComputeNextDirectionEaten(map<Map::Direction, Vector2>, Map::Direction) override;
		//
		Vector2 GetStartPosition() override;
		Vector2 GetFrightenedPosition() override;
		Vector2 GetEatenPosition() override;

	public:
		BlinkyGhost(Map* map, Player* player);
		void CollisionWith(Actor* actor) override;
		~BlinkyGhost() override;
};
