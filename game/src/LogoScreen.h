#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "ScreenData.h"

// Screen shown at the beginning.
class LogoScreen : public Screen
{
	private:
		Texture2D _image;
		float _imageAspectRatio;
		Rectangle _source;
		Rectangle _dest;
		// Time this screen will be active
		float _time;
		float _progress;

	public:
		LogoScreen(ScreensManager*, ScreenData* = nullptr);
		void Update() override;
		void Draw() override;
};
