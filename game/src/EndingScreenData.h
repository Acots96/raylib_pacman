#pragma once
#include "ScreenData.h"

class EndingScreenData : public ScreenData
{
	public:
		bool playerWin;
		explicit EndingScreenData(bool win) { playerWin = win; }
};