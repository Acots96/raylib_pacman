#include "Pacman.h"
#include <iostream>
#include "LogoScreen.h"
#include "TitleScreen.h"
#include "OptionsScreen.h"
#include "GameplayScreen.h"
#include "EndingScreen.h"
#include "ResourcesManager.h"

Pacman::Pacman()
{
	ResourcesManager::LoadResources();

	_activeScreen = nullptr;

	SwitchTo(LOGO);
}

void Pacman::SwitchTo(int screen, ScreenData* data)
{
	if (_activeScreen != nullptr)
		Engine::Destroy(_activeScreen);

	switch (screen)
	{
		case 0:
			_activeScreen = new LogoScreen(this, data);
			break;
		case 1:
			_activeScreen = new TitleScreen(this, data);
			break;
		case 2:
			_activeScreen = new OptionsScreen(this, data);
			break;
		case 3:
			_activeScreen = new GameplayScreen(this, data);
			break;
		case 4:
			_activeScreen = new EndingScreen(this, data);
			break;
		default:
			break;
	}
	Engine::Instantiate(_activeScreen);
}

Pacman::~Pacman()
{
	ResourcesManager::UnloadResources();
}



///////////////////////////////////////


int main(void)
{
	const int screenWidth = 800;
	const int screenHeight = 800;

	Engine::Start(screenWidth, screenHeight, 60, "Pacman!");
	Engine::Instantiate(new Pacman());
	//
	Engine::Execute();
}
