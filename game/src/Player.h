#pragma once
#include "Actor.h"
#include <vector>
#include "Map.h"

class ResourcesManager;
class GameManager;

// Class representing the actor of the game controlled 
// by the user
class Player : public Actor
{
	private:
		Texture2D _player;
		Texture2D _playerDead;
		int _cellSize;
		// List of sprites of the player's character:
		// One list per direction (4) and dead sprites.
		vector<vector<Rectangle>> _rectangles;
		//
		Vector2 _position;
		Vector2 _lastPosition;
		Rectangle _body;
		Map::Direction _direction;
		float _speed;
		//
		Map* _map;
		GameManager* _manager;
		int _score;
		int _lifes;
		int _pillsEaten;
		// Animation data
		short _sprite;
		float _timeBetweenSprites;
		float _deltaTimeBetweenSprites;
		bool _isWakaWakaPlaying;
		// Die animation data
		bool _doDieEffect;
		float _waitToStartDeath;
		float _timePerDeadSprite;
		float _deltaDeathTime;

	public:
		explicit Player(Map* map, GameManager* manager);
		void Update() override;
		void Draw() override;
		Rectangle GetBody() override;
		void CollisionWith(Actor*) override;
		~Player() override;
		//
		void Reset();
		int GetScore();
		int GetLifes();
		Vector2 GetPosition();
		void Die();
};
