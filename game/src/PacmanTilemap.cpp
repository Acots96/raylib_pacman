#include "PacmanTilemap.h"
#include <iostream>
#include "ResourcesManager.h"

PacmanTilemap::PacmanTilemap() : Tilemap(ResourcesManager::PacmanTilesetImage,
	ResourcesManager::TilesMap, ResourcesManager::CollidersMap, ResourcesManager::ObjectsMap
) { }

std::vector<Rectangle> PacmanTilemap::GetRectanglesFromTexture()
{
	int imageRows = 3;
	int imageCols = 10;

	int rowsCellSize = tilesSamples.height / imageRows;
	int colsCellSize = tilesSamples.width / imageCols;

	cout << rowsCellSize << "=" << tilesSamples.height << "/" << imageRows << "\n";
	cout << colsCellSize << "=" << tilesSamples.width << "/" << imageCols << "\n";

	std::vector<Rectangle> rects;
	rects.reserve(imageRows * imageCols + 1);

	rects.push_back({});

	// First 9 sprites: thin borders.
	for (int y = 0; y <= 2; y++)
	{
		for (int x = 0; x <= 2; x++)
		{
			Rectangle r = { x * colsCellSize, y * rowsCellSize, rowsCellSize, colsCellSize };
			rects.push_back(r);
		}
	}

	// Next 9 sprites: thick corner borders.
	for (int y = 0; y <= 2; y++)
	{
		for (int x = 3; x <= 5; x++)
		{
			Rectangle r = { x * colsCellSize, y * rowsCellSize, rowsCellSize, colsCellSize };
			rects.push_back(r);
		}
	}

	// Next 3 sprites: vertical thick borders.
	for (int y = 0; y <= 2; y++)
	{
		for (int x = 6; x <= 6; x++)
		{
			Rectangle r = { x * colsCellSize, y * rowsCellSize, rowsCellSize, colsCellSize };
			rects.push_back(r);
		}
	}

	// Next 9 sprites: Rest of them
	for (int y = 0; y <= 2; y++)
	{
		for (int x = 7; x <= 9; x++)
		{
			Rectangle r = { x * colsCellSize, y * rowsCellSize, rowsCellSize, colsCellSize };
			rects.push_back(r);
		}
	}

	for (int i = 0; i < rects.size(); i++)
	{
		Rectangle r = rects[i];
		cout << r.x << "," << r.y << "," << r.width << "," << r.height << "\n";
	}

	return rects;
}