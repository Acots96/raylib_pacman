#pragma once
#include "Actor.h"
#include <memory>
#include <array>
#include <vector>
#include <map>
#include <string>

using namespace std;
class GameManager;
class Tilemap;

class Map : public Actor
{
	public:
		enum ObjectType { None = 0, Bigpill = 28, Pill = 30 };
		enum Direction { Stop = -1, Right = 0, Left, Up, Down };

	private:
		Tilemap* _tilemap;
		GameManager* _manager;
		//
		Vector2 _start;
		int _cellSize;
		int _rows;
		int _columns;
		Rectangle _screenRect;
		vector<Rectangle> _rectangles;
		//
		vector<int> _tiles;
		vector<int> _objects;
		int _pillsAmount;
		//
		map<int, int> _tunnels;

	public:
		Map(Vector2 startPos, int cellSize, Tilemap* tilemap, GameManager* manager);
		void Draw() override;
		// Given the row and column positions in the tilemap,
		// returns the real position in pixels.
		Vector2 TileToMapPosition(int row, int col);
		// Gives the direction as a Vector2 (range: [-1,1])
		// with the given Direction.
		Vector2 ComputeDirection(Direction);
		// Given the position, returns all the walkable/reachable
		// tiles surrounding, specifying each Direction.
		map<Direction, Vector2> GetAvailableAdjacent(Vector2 position,
			bool includeNegative = false);
		Map::Direction Map::GetOppositeDirection(Map::Direction);
		// Checks if nextPosition is reachable and returns it,
		// otherwise returns position.
		Vector2 ComputeNextPosition(Vector2 position, Vector2 nextPosition);
		// Returns the object in the given position and removes
		// it if removeIfConsumible is true.
		ObjectType CheckObject(Vector2 position, bool removeIfConsumible = true);
		int GetCellSize();
		int GetPillsAmount();
		//
		static float Distance(Vector2, Vector2);
};
